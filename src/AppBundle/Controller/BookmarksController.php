<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Bookmark;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class BookmarksController extends FOSRestController
{
    /**
     * @Annotations\Get()
     *
     * @param Request $request
     * @return array
     */
    public function listAction(Request $request)
    {
        $bookmarks = $this->getDoctrine()
            ->getRepository('AppBundle:Bookmark')
            ->findBy([], ['createdAt' => 'DESC'], 10);

        /** @var $bookmark Bookmark */
        foreach ($bookmarks as $bookmark) {
            $bookmark->removeComments();
        }

        return array('bookmarks' => $bookmarks);
    }

    /**
     * @Annotations\Get()
     *
     * @param Request $request
     * @return array
     */
    public function showAction(Request $request)
    {
        $url = $request->query->get('url');

        if (!$url) {
            throw new BadRequestHttpException('Please specify url');
        }

        $bookmark = $this->getDoctrine()
            ->getRepository('AppBundle:Bookmark')
            ->findOneBy(['url' => $url]);

        if (!$bookmark) {
            throw new NotFoundHttpException('Bookmark for requested url was not found');
        }

        foreach ($bookmark->getComments() as $comment)
        {
            $comment->setBookmark(null);
        }

        return array('bookmark' => $bookmark);
    }

    /**
     * @Annotations\Post()
     *
     * @param Request $request
     * @return array
     */
    public function addAction(Request $request)
    {
        $url = $request->request->get('url');

        if (!$url) {
            throw new BadRequestHttpException('Please specify url');
        }

        $bookmark = $this->getDoctrine()
            ->getRepository('AppBundle:Bookmark')
            ->findOneBy(['url' => $url]);

        if (!$bookmark) {
            $bookmark = new Bookmark();
            $bookmark->setUrl($url);
            $bookmark->setCreatedAt(new \DateTime());

            $em = $this->getDoctrine()->getManager();
            $em->persist($bookmark);
            $em->flush();
        }

        return array('id' => $bookmark->getId());
    }
}
