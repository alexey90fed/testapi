<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Comment;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class CommentsController extends FOSRestController
{
    /**
     * @Annotations\Post()
     *
     * @param Request $request
     * @return array
     */
    public function createAction(Request $request)
    {
        $bookmarkId = (int) $request->request->get('bookmarkId');
        $text = $request->request->get('text');

        if (!$bookmarkId) {
            throw new BadRequestHttpException('Bookmark id should be integer');
        }
        if (!$text) {
            throw new BadRequestHttpException('Please specify comment text');
        }

        $bookmark = $this->getDoctrine()
            ->getRepository('AppBundle:Bookmark')
            ->findOneBy(['id' => $bookmarkId]);

        if (!$bookmark) {
            throw new NotFoundHttpException('Bookmark does not exist');
        }

        $comment = new Comment();
        $comment->setBookmark($bookmark);
        $comment->setText($text);
        $comment->setIp($request->getClientIp());
        $comment->setCreatedAt(new \DateTime());

        $em = $this->getDoctrine()->getManager();
        $em->persist($comment);
        $em->flush();

        return array('commentId' => $comment->getId());
    }

    /**
     * @Annotations\Put()
     *
     * @param Request $request
     * @return array
     */
    public function updateAction(Request $request)
    {
        $commentId = (int) $request->request->get('commentId');
        $text = $request->request->get('text');
        $ip = $request->getClientIp();

        if (!$commentId) {
            throw new BadRequestHttpException('Comment id should be integer');
        }
        if (!$text) {
            throw new BadRequestHttpException('Please specify comment text');
        }

        /** @var $comment Comment */
        $comment = $this->getDoctrine()
            ->getRepository('AppBundle:Comment')
            ->findOneBy(['id' => $commentId]);

        if (!$comment) {
            throw new NotFoundHttpException('Comment does not exist');
        }
        if ($comment->getIp() != $ip) {
            throw new BadRequestHttpException('This IP-address is not allowed to update comment');
        }
        if ($comment->getCreatedAt(false) < new \DateTime('-1 hour')) {
            throw new BadRequestHttpException('1 hour had passed since comment was added');
        }

        $comment->setText($text);

        $em = $this->getDoctrine()->getManager();
        $em->persist($comment);
        $em->flush();

        return array('commentId' => $comment->getId());
    }

    /**
     * @Annotations\Delete()
     *
     * @param Request $request
     * @return array|\Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function removeAction(Request $request)
    {
        $commentId = (int) $request->request->get('commentId');
        $ip = $request->getClientIp();

        if (!$commentId) {
            throw new BadRequestHttpException('Comment id should be integer');
        }

        $comment = $this->getDoctrine()
            ->getRepository('AppBundle:Comment')
            ->findOneBy(['id' => $commentId]);

        if (!$comment) {
            throw new NotFoundHttpException('Comment does not exist');
        }
        if ($comment->getIp() != $ip) {
            throw new BadRequestHttpException('This IP-address is not allowed to remove comment');
        }
        if ($comment->getCreatedAt(false) < new \DateTime('-1 hour')) {
            throw new BadRequestHttpException('1 hour had passed since comment was added');
        }

        $em = $this->getDoctrine()->getManager();
        $em->remove($comment);
        $em->flush();

        return array(true);
    }
}
