# Installation

Clone project

```sh
git clone https://alexey90fed@bitbucket.org/alexey90fed/testapi.git
```

Open project directory.
```sh
cd testapi
```

Run composer install.
See how to install composer here https://getcomposer.org/download/.
```sh
composer install
```

Make cache and logs directories be available for writing.
```sh
chmod 777 -R app/cache/ app/logs/
```

Create database and tables.
```sh
php app/console doctrine:database:create
php app/console doctrine:schema:create
```

Configure virtual host.
See how to do that here http://symfony.com/doc/current/cookbook/configuration/web_server_configuration.html.


# API Documentation

### Bookmarks

##### GET /bookmark/list

Gets 10 last bookmarks.

##### GET /bookmark/show

Gets bookmark with comments by url.

Params:

  - {url} - url of bookmark

##### POST /bookmark/add

Creates bookmark for specified url. Returns its id.
If bookmark with this url already exists, returns its id.

Params:

  - {url} - url which we need to bookmark

### Comments

##### POST /comment/create

Creates comment for specified by id bookmark. Returns comment id.

Params:

  - {bookmarkId} - id of bookmark that we need add comment to
  - {text} - text of comment

##### PUT /comment/update

Updates comment that is specified by id.
Comment can be updated only from the same IP-address that it was created.
Comment can't be updated after hour passed since its creation.

Params:

  - {commentId} - id of comment that we need to update
  - {text} - new text of comment

##### DELETE /comment/remove

Removes comment that is specified by id.
Comment can be removed only from the same IP-address that it was created.
Comment can't be removed after hour passed since its creation.

Params:

  - {commentId} - id of comment that we need to update
